const prompt = require('prompt-sync')();

let n = parseFloat(prompt("Kerro luku: "));

let sum_2 = 0;
let c = 0;

do {
    if (c % 3 === 0) {
        console.log("Fizz");
    }
    if (c % 5 === 0) {
        console.log("Buzz");
    } 
    if ((c % 5 === 0) && (c % 3 === 0)) {
        console.log("FizzBuzz");
    }
    c = c + 1;
} while (c <= n);