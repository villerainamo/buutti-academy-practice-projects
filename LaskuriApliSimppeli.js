const prompt = require('prompt-sync')();

console.log("Syötä kaksi lukua ja haluamasi laskutoimitus: ");

let luku_1 = parseFloat(prompt("Luku 1: "));
let luku_2 = parseFloat(prompt("Luku 2: "));
let laskutoimitus = prompt("Laskutoimitus: ");

let tulos = 0;

if (laskutoimitus.includes("+")) {
    tulos = tulos + luku_1 + luku_2;
} else if (laskutoimitus.includes("-")) {
    tulos = tulos + luku_1 - luku_2;
} else if (laskutoimitus.includes("/")) {
    tulos = tulos + (luku_1 / luku_2);
} else if (laskutoimitus.includes("*")) {
    tulos = tulos + (luku_1 * luku_2);
}

console.log(tulos);