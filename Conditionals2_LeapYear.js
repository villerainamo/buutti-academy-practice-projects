const prompt = require('prompt-sync')();

let year = parseFloat(prompt("What year is it?: "));

if (year % 4 === 0) {
    if (year % 100 === 0) {
        if (year % 400 === 0) {
            console.log("It's a leap year.");
        } else {
            console.log("It's not a leap year.");
        }
    } else {
        console.log("It's a leap year.");
    }
} else {
    console.log("It's not a leap year.");
}
