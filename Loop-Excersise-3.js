const prompt = require('prompt-sync')();

let n = parseFloat(prompt("Kerro luku: "));

let sum_2 = 0;
let c = 0;

do {
    if ((c % 3 === 0) || (c % 5 === 0)) {
        sum_2 = sum_2 + c;
        console.log(c);
    }
    c = c + 1;
} while (c <= n);

console.log(sum_2);