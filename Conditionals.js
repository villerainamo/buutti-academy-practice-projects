const prompt = require('prompt-sync')();

let pelaajat = parseFloat(prompt("Anna pelaajien määrä: "));

if (pelaajat !== 4) {
    console.log("Tarvitaan neljä pelaajaa.");
} else {
    console.log("Hyvää peliä!");
}

let mark_status_stressed = prompt("Is Mark stressed?: ");
let mark_status_icecream = prompt("Does Mark have ice cream?: ");

if ((mark_status_stressed === "yes" && mark_status_icecream === "yes" ) || (mark_status_stressed === "no" && mark_status_icecream === "no") || (mark_status_icecream === "yes" && mark_status_stressed === "no")) {
    console.log("Mark is happy.");
} else {
    console.log("Mark is not happy.");
}

let sun_status = prompt("Is the sun shining?: ");
let rain_status = prompt("Is it raining?: ");
let temperature = parseFloat(prompt("What is the temperature?: "));

if (sun_status === "yes" && rain_status === "no" && temperature > 20) {
    console.log("It's a beach day!");
} else {
    console.log("Beach day it is not...");
}

let seen_suzy = prompt("Did Arin see Suzy?: ");
let seen_dan = prompt("Did Arin see Dan?: ");

if ((seen_dan === "yes" || seen_suzy === "yes") && (seen_dan === "no" || seen_suzy === "no")) {
    console.log("Arin is happy.");
} else {
    console.log("Arin is not happy...");
}