const prompt = require('prompt-sync')();

const str_1 = prompt("Anna merkkijono: ");
const str_2 = prompt("Anna toinen merkkijono: ");

const str_sum = `${str_1}${str_2}`;

console.log(typeof str_sum);

console.log(str_sum);

console.log(str_1.length);
console.log(str_2.length);

str_avg = (str_1.length + str_2.length) / 2;
console.log(str_avg);

if (str_1.length < str_avg) {
    console.log(str_1);
}

if (str_2.length < str_avg) {
    console.log(str_2);
}

if (str_sum.length < str_avg) {
    console.log(str_sum);
}
