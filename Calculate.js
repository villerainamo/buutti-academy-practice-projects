const prompt = require('prompt-sync')();

let numero_1 = parseFloat(prompt("Anna eka numero"));
let numero_2 = parseFloat(prompt("Anna toka numero"));

let numero_3 = calculate_sum(numero_1,numero_2);
let numero_4 = calculate_difference(numero_1, numero_2);
let numero_5 = calculate_fraction(numero_1, numero_2);
let numero_6 = calculate_product(numero_1, numero_2);
let numero_7 = calculate_exponent(numero_1, numero_2);
let numero_8 = calculate_product(numero_1, numero_2);

function calculate_sum(number1, number2) {
    return number1 + number2;
}

function calculate_difference(number1, number2) {
    return number1 - number2;
}

function calculate_fraction(number1, number2) {
    return number1 / number2;
}

function calculate_product(number1, number2) {
    return number1 * number2;
}

function calculate_exponent(number1, number2) {
    return number1 ** number2;
}

function calculate_modulo(number1, number2) {
    return number1 % number2;
}

console.log(numero_3);
console.log(numero_4);
console.log(numero_5);
console.log(numero_6);
console.log(numero_7);
console.log(numero_8);
